import Cocoa

//Variables utilizadas
var cadena: String
var añosRomanos: [Int] = []
var letras: [String] = []
var numerosString: [String]
var numeros: [Int] = []
var chequeo = false
var patron: String
var formatoCorrecto: [String]
var suma = 0
var resultado = 0

func matches(for patron: String, in usuario: String) -> [String] {
//Funcion regex
    do {
        let regex = try NSRegularExpression(pattern: patron)
        let resultado = regex.matches(in: usuario, range: NSRange(usuario.startIndex..., in: usuario))
        
        return resultado.map {
            String(usuario[Range($0.range, in: usuario)!])
        }
    } catch {
        return []
    }
}

//Instrucciones
print("Ejercicio 2")
print("")
print("En este ejercicio me proporcionará un intervalo de años en gregoriano y yo le proporcionare la minima cantidad equivalente de caracteres necesarios para todos los años en romano")
print("")
print("Ejem: 1BC - 1AD")
print("Donde (0...753)BC y (0...2012)AD")
print("")
print("Deme el rango de años")
//cadena = readLine() ?? "0"
cadena = "auais99@hotmail.com"
patron = "[\\d\\w\\._-]+@[A-Z]+\\.[A-Z]{2,}"
patron = "[\\d\\w._-]+@[\\w]+.[\\w]{2,}"
print(matches(for: patron, in: cadena))

/*
while !chequeo { //Ciclar hasta que el valor escrito sea valido
    
    patron = "\\d{0,4}(AD|BC)\\s-\\s\\d{0,4}(AD|BC)" //patron para checar sintaxis
    formatoCorrecto = matches(for: patron, in: cadena)
    
    if formatoCorrecto != [] {
        
        patron = "\\d+" //patron para almacenar los años
        numerosString = matches(for: patron, in: formatoCorrecto[0])
        patron = "(AD|BC)" //patron para almacenar AD o BC
        letras = matches(for: patron, in: formatoCorrecto[0])
        
        numeros.append(Int(numerosString[0]) ?? 0)
        numeros.append(Int(numerosString[1]) ?? 0)
        
        if !(letras[0] == "AD" && letras[1] == "BC") //Checa si esta correcto el orden
        {
            for i in 0...1 { //Almacena los años en su equivalencia romana y evalua rangos
                if letras[i] == "BC" && numeros[0] > 0 && numeros[0] < 754 {
                    añosRomanos.append(754 - numeros[i])
                    chequeo = true
                } else {
                    if letras[i] == "AD" && numeros[0] > 0 && numeros[0] < 2013 {
                        añosRomanos.append(753 + numeros[i])
                        chequeo = true
                    } else {
                        print("Porfavor escriba el intervalo de la manera requerida: ")
                        cadena = readLine() ?? "0"
                    }
                }
            }
        } else {
            print("Porfavor escriba el intervalo de la manera requerida: ")
            cadena = readLine() ?? "0"
        }
    } else {
        print("Porfavor escriba el intervalo de la manera requerida: ")
        cadena = readLine() ?? "0"
    }
}

patron = "\\d" //Patron para separar los digitos

for i in añosRomanos[0]...añosRomanos[1] { // Cicla todo el intervalo de años
    numerosString = matches(for: patron, in: String(i))
    for j in 0...(numerosString.count - 1) { //Dependiendo del numero es la cantidad de
        switch numerosString[j] {            //caracteres que necesita
        case "1", "5":
            suma += 1
        case "2", "4", "6", "9":
            suma += 2
        case "3", "7":
            suma += 3
        case "8":
            suma += 4
        default:
            suma += 0
        }
    }
    if resultado < suma { //Checa si es mayor el amacenado con el actual
        resultado = suma
    }
    suma = 0
}
print("")
print(resultado)
*/
